import React, { useEffect, useState } from "react";
import DetailComponent from "../component/DetailComponent";
import { useSearchParams } from "react-router-dom";
function Detail() {
  const [searcParams] = useSearchParams();
  const [data, setdata] = useState();
  const callApi = () => {
    fetch("https://dummyjson.com/products/" + searcParams.get("id"))
      .then((res) => res.json())
      .then((result) => {
        setdata(result);
        console.log(result);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    callApi();
  }, []);
  return (
    <DetailComponent
      title={data?.title}
      img={data?.images[0]}
      desc={data?.description}
      price={"Rp" + data?.price}
      rate={data?.rating}
      discountPercentage={data?.discountPercentage}
      stock={data?.stock}
    />
  );
}

export default Detail;
