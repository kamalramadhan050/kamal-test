import React, { useEffect, useState } from "react";
import "../App.css";
import ListComponent from "../component/ListComponent";
import PaginantionComponent from "../component/PaginantionComponent";

function Home() {
  const [data, setdata] = useState();
  const [status, setStatus] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [postPerPage, setPostPerPage] = useState(5);

  const callApi = () => {
    fetch("https://dummyjson.com/products")
      .then((res) => res.json())
      .then((result) => {
        const indexOfLast = currentPage * postPerPage;
        const indexOfFirst = indexOfLast - postPerPage;
        const currentPosts = result.products?.slice(indexOfFirst, indexOfLast);
        console.log(currentPosts);
        setdata(currentPosts);
        console.log(data.length);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    callApi();
    return () => {
      callApi();
    };
  }, [currentPage]);

  const handleSort = () => {
    console.log("click");
    if (status) {
      const strAscending = [...data].sort((a, b) =>
        a.title > b.title ? 1 : -1
      );
      setdata(strAscending);
      setStatus(!status);
    } else {
      const strDescending = [...data].sort((a, b) =>
        a.title > b.title ? -1 : 1
      );
      setdata(strDescending);
      setStatus(!status);
    }
  };

  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  return (
    <div className="App">
      <button onClick={handleSort} className="btn">
        {status ? "ascending" : "descending"}
      </button>
      <ListComponent data={data} />
      <PaginantionComponent
        postPerPage={postPerPage}
        totalPosts={data?.length}
        paginate={paginate}
      />
    </div>
  );
}

export default Home;
