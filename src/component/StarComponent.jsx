import React from "react";

function StarComponent(props) {
  const intStar = parseInt(props.star)
  const starArray = [...Array(intStar).keys()].map((i) => i + 1);
  return (
    <div>
      {starArray.map((i) => (
        <span className="star">&#9733;</span>
      ))}
    </div>
  );
}

export default StarComponent;
