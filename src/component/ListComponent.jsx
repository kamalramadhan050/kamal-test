import React from "react";
import "./ListComponent.css";
import StarComponent from "./StarComponent";
import { createSearchParams, Link, useNavigate } from "react-router-dom";
import PriceComponent from "./PriceComponent";
const ListComponent = (props) => {
  const navigate = useNavigate();
  return (
    <div>
      {props.data?.map((item, i) => (
        <div key={item?.id}>
          <div
            className="list-wrapper"
            onClick={() =>
              navigate({
                pathname: "/detail",
                search: createSearchParams({
                  id: item?.id,
                }).toString(),
              })
            }
          >
            <div className="body">
              <img
                src={item.images[0]}
                alt=""
                width="300"
                style={{ marginRight: 10 }}
              />
              <div>
                <p className="title">{item?.title}</p>
                <p className="desc">{item?.description}</p>
                <div className="body-rate">
                  <p className="rate">{item?.rating}</p>
                  <StarComponent star={item?.rating.toFixed(0)} />
                </div>
              </div>
            </div>
                <PriceComponent price={item?.price} discount={item?.discountPercentage}/>
          </div>
          <hr />
        </div>
      ))}
    </div>
  );
};

export default ListComponent;
