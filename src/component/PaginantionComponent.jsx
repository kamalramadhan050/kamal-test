import React from "react";
import "./PaginationComponent.css";
function PaginantionComponent(props) {
  const pageNumber = [1, 2, 3, 4, 5];
  return (
    <nav>
      <ul className="pagination">
        {pageNumber?.map((number) => (
          <li key={number} className="page-item">
            <a onClick={() => props?.paginate(number)} className="page-link">
              {number}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
}

export default PaginantionComponent;
