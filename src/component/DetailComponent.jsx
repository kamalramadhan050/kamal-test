import React from "react";

function DetailComponent(props) {
  return (
    <div className="list-wrapper">
      <div className="body">
        <img src={props.img} alt="" />
        <div>
          <p className="title">Nama: {props.title}</p>
          <p className="desc">Deskripsi: {props.desc}</p>
          <div className="body-rate">
            <p className="rate">Rating: {props.rate}</p>
            <span className="star">&#9733;</span>
          </div>
          <p className="price">Harga: {props.price}</p>
          <p className="stock">Stock: {props.stock}</p>
          <p className="discount">Diskon: {props.discountPercentage}%</p>
        </div>
      </div>
    </div>
  );
}

export default DetailComponent;
