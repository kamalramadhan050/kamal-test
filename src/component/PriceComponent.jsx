import React from "react";

function PriceComponent(props) {
  const totalValue = props.price * ( (100-props.discount) / 100 )
  return (
    <div>
      <p className="price" style={{textDecoration: 'line-through'}}>${props.price}</p>
      <p className="discount">${totalValue.toFixed(0)}</p>
    </div>
  );
}

export default PriceComponent;
